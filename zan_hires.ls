$author "Ketmar Dark, Hedon developers"
$title "Zan: Journey to Another Dimension, version 1.3 (hires sprites)"
$commentfile credits.txt
// start with `!` to be relative to script dir
$dest !../player_zan_hires.vwad

$filegroup "brightmaps"
$recursedir brightmaps/  brightmaps/

$filegroup "decorate scripts"
$recursedir decorate/    decorate/

$filegroup "graphics"
$recursedir graphics/    graphics/
$recursedir sprites/     sprites_common/
$recursedir sprites/     sprites_hires/

$filegroup "models"
$recursedir models/      models/

$filegroup "progs"
$recursedir progs/       progs/

$filegroup "sounds"
$recursedir sounds/      sounds/


$filegroup "decorate scripts"
decorate.txt  decorate.txt

$filegroup "various definitions"
cvarinfo.txt  cvarinfo.txt
decaldef.txt  decaldef.txt
gldefs.txt    gldefs.txt
keyconf.txt   keyconf.txt
//loadvcc.txt   loadvcc.txt
//loadvcs.txt   loadvcs.txt
modmenu.txt   modmenu.txt
sndinfo.txt   sndinfo.txt
terrain.txt   terrain.txt
textures.txt  textures.txt

vavoomc_mods.rc  vavoomc_mods.rc

$filegroup "credits"
credits.txt   credits.txt
