Sprite "AXEAA0", 1060, 605
{
  XScale 3.000
  YScale 3.600
  Patch "AXEAA0", 593, 107
}

Sprite "AXEAB0", 960, 605
{
  XScale 3.000
  YScale 3.600
  Patch "AXEAB0", 525, 6
}

Sprite "AXEAC0", 960, 605
{
  XScale 3.000
  YScale 3.600
  Patch "AXEAC0", 502, 419
}

Sprite "AXEAD0", 960, 605
{
  XScale 3.000
  YScale 3.600
  Patch "AXEAD0", 691, 483
}

Sprite "AXEFA0", 1200, 700
{
  XScale 3.000
  YScale 4.000
  Patch "AXEFA0", 598, 530
}

Sprite "AXEFB0", 1200, 700
{
  XScale 3.000
  YScale 4.100
  Patch "AXEFB0", 479, 386
}

Sprite "AXEFC0", 960, 720
{
  XScale 3.000
  YScale 4.200
  Patch "AXEFC0", 395, 257
}

Sprite "AXEFD0", 960, 720
{
  XScale 3.000
  YScale 4.200
  Patch "AXEFD0", 276, 183
}

Sprite "AXEFE0", 960, 700
{
  XScale 3.000
  YScale 4.100
  Patch "AXEFE0", 144, 206
}

Sprite "AXEFF0", 960, 700
{
  XScale 3.000
  YScale 4.000
  Patch "AXEFF0", 0, 307
}

Sprite "AXEFH0", 1200, 705
{
  XScale 3.000
  YScale 4.200
  Offset 240, 0
  Patch "AXEFH0", 10, 453
}

Sprite "AXEFI0", 1200, 730
{
  XScale 3.000
  YScale 4.300
  Offset 200, 0
  Patch "AXEFI0", 143, 306
}

Sprite "AXEFJ0", 960, 730
{
  XScale 3.000
  YScale 4.300
  Patch "AXEFJ0", 281, 226
}

Sprite "AXEFK0", 960, 705
{
  XScale 3.000
  YScale 4.200
  Patch "AXEFK0", 677, 332
}

Sprite "WSPIA0", 960, 605
{
  XScale 3.000
  YScale 3.600
  Patch "WSPIA0", 499, 359
}

Sprite "WSPFA0", 960, 700
{
  XScale 3.000
  YScale 3.600
  Patch "WSPFA0", 505, 369
}

Sprite "WSPFB0", 960, 700
{
  XScale 3.000
  YScale 3.600
  Patch "WSPFB0", 511, 379
}

Sprite "WSPFC0", 960, 700
{
  XScale 3.000
  YScale 3.600
  Patch "WSPFC0", 502, 364
}

Sprite "WSPSA0", 960, 600
{
  XScale 3.000
  YScale 3.000
  Offset 0, 2
  Patch "WSPSA0", 16, 204
}

Sprite "WSPRA0", 960, 600
{
  XScale 3.000
  YScale 3.000
  Offset 1, 0
  Patch "WSPRA0", 581, 96
}

Sprite "WSPRB0", 960, 600
{
  XScale 3.000
  YScale 3.000
  Offset 0, 32
  Patch "WSPRB0", 656, -1
}

Sprite "WSPRC0", 960, 600
{
  XScale 3.000
  YScale 3.000
  Offset 2, 49
  Patch "WSPRC0", 548, 16
}

Sprite "WSPRD0", 960, 600
{
  XScale 3.000
  YScale 3.000
  Offset 0, 53
  Patch "WSPRD0", 510, 20
}

Sprite "WSPRE0", 960, 600
{
  XScale 3.000
  YScale 3.000
  Offset 1, 43
  Patch "WSPRE0", 617, 19
}

Sprite "WSPRF0", 960, 600
{
  XScale 3.000
  YScale 3.000
  Offset 0, 53
  Patch "WSPRF0", 715, 19
}

Sprite "WSPRG0", 960, 600
{
  XScale 3.000
  YScale 3.000
  Offset 0, 53
  Patch "WSPRG0", 561, 258
}

Sprite "WFRRA0", 960, 600
{
  XScale 3.000
  YScale 3.500
  Offset 0, 4
  Patch "WFRRA0", 188, 379
}

Sprite "WFRRB0", 960, 600
{
  XScale 3.000
  YScale 3.500
  Offset 0, 4
  Patch "WFRRB0", 188, 379
}

Sprite "WFRFA0", 960, 600
{
  XScale 3.000
  YScale 3.500
  Offset 0, 4
  Patch "WFRFA0", 256, 387
}

Sprite "WFRFB0", 960, 700
{
  XScale 3.000
  YScale 3.500
  Offset 0, 4
  Patch "WFRFB0", 292, 402
}

Sprite "WFRFC0", 960, 700
{
  XScale 3.000
  YScale 3.500
  Offset 0, 4
  Patch "WFRFC0", 281, 424
}

Sprite "WFRFD0", 1060, 720
{
  XScale 3.000
  YScale 3.500
  Offset 0, 4
  Patch "WFRFD0", 207, 477
}

Sprite "WFRLA0", 960, 600
{
  XScale 3.000
  YScale 3.500
  Offset 0, 4
  Patch "WFRLA0", 225, 348
}

Sprite "WFRLB0", 960, 600
{
  XScale 3.000
  YScale 3.500
  Offset 0, 4
  Patch "WFRLB0", 294, 337
}

Sprite "WFRLC0", 960, 600
{
  XScale 3.000
  YScale 3.500
  Offset 0, 4
  Patch "WFRLC0", 270, 345
}

Sprite "WFRLD0", 960, 600
{
  XScale 3.000
  YScale 3.500
  Offset 0, 4
  Patch "WFRLD0", 495, 344
}

Sprite "WFRLE0", 960, 600
{
  XScale 3.000
  YScale 3.500
  Offset 0, 4
  Patch "WFRLE0", 496, 344
}

Sprite "WFRLF0", 960, 600
{
  XScale 3.000
  YScale 3.500
  Offset 0, 4
  Patch "WFRLF0", 510, 342
}

Sprite "WFRLG0", 960, 600
{
  XScale 3.000
  YScale 3.500
  Offset 0, 4
  Patch "WFRLG0", 222, 352
}

Sprite "WFIFA0", 960, 700
{
  XScale 3.000
  YScale 3.800
  Offset -2, -84
  Patch "WFIFA0", 669, 418
}

Sprite "WFIFB0", 960, 700
{
  XScale 3.000
  YScale 3.800
  Offset 0, -90
  Patch "WFIFB0", 527, 318
}

Sprite "WFIFC0", 960, 700
{
  XScale 3.000
  YScale 3.800
  Offset 0, -90
  Patch "WFIFC0", 395, 247
}

Sprite "WFIFD0", 960, 700
{
  XScale 3.000
  YScale 3.800
  Offset 0, -63
  Patch "WFIFD0", -15, 387
}

Sprite "WFIFE0", 960, 700
{
  XScale 3.000
  YScale 3.800
  Offset 0, -63
  Patch "WFIFE0", 58, 334
}

Sprite "WFIFF0", 960, 700
{
  XScale 3.000
  YScale 3.800
  Offset 0, -63
  Patch "WFIFF0", 87, 266
}

Sprite "WKICA0", 960, 700
{
  XScale 3.000
  YScale 3.800
  Offset -2, -28
  Patch "WKICA0", 313, 552
}

Sprite "WKICB0", 960, 700
{
  XScale 3.000
  YScale 3.800
  Offset -2, -28
  Patch "WKICB0", 309, 416
}

Sprite "WKICC0", 960, 700
{
  XScale 3.000
  YScale 3.800
  Offset -2, -28
  Patch "WKICC0", 338, 284
}

Sprite "WKICD0", 960, 700
{
  XScale 3.000
  YScale 3.800
  Offset -2, -28
  Patch "WKICD0", 367, 165
}

Sprite "WKICE0", 960, 700
{
  XScale 3.000
  YScale 3.800
  Offset -2, -28
  Patch "WKICE0", 245, 387
}

Sprite "WKICF0", 960, 700
{
  XScale 3.000
  YScale 3.800
  Offset -2, -28
  Patch "WKICF0", 272, 482
}

Sprite "WKICG0", 960, 700
{
  XScale 3.000
  YScale 3.800
  Offset -2, -28
  Patch "WKICG0", 523, 556
}

Sprite "WAKIA0", 960, 700
{
  XScale 3.000
  YScale 3.800
  Offset -2, -28
  Patch "WAKIA0", 54, 501
}

Sprite "WAKIB0", 960, 700
{
  XScale 3.000
  YScale 3.800
  Offset -2, -28
  Patch "WAKIB0", 155, 381
}

Sprite "WAKIC0", 960, 700
{
  XScale 3.000
  YScale 3.800
  Offset -2, -28
  Patch "WAKIC0", 223, 195
}

Sprite "WAKID0", 960, 700
{
  XScale 3.000
  YScale 3.800
  Offset -2, -28
  Patch "WAKID0", 332, 243
}

Sprite "WAKIE0", 960, 700
{
  XScale 3.000
  YScale 3.800
  Offset -2, -28
  Patch "WAKIE0", 436, 301
}

Sprite "WAKIF0", 960, 700
{
  XScale 3.000
  YScale 3.800
  Offset -2, -28
  Patch "WAKIF0", 551, 478
}

Sprite "WAKIG0", 960, 700
{
  XScale 3.000
  YScale 3.800
  Offset -2, -28
  Patch "WAKIG0", 685, 566
}

Sprite "WSKIA0", 960, 700
{
  XScale 3.000
  YScale 3.800
  Offset -2, -28
  Patch "WSKIA0", 602, 448
}

Sprite "WSKIB0", 960, 700
{
  XScale 3.000
  YScale 3.800
  Offset -2, -28
  Patch "WSKIB0", 513, 330
}

Sprite "WSKIC0", 960, 700
{
  XScale 3.000
  YScale 3.800
  Offset -2, -28
  Patch "WSKIC0", 347, 303
}

Sprite "WSKID0", 960, 700
{
  XScale 3.000
  YScale 3.800
  Offset -2, -28
  Patch "WSKID0", 286, 355
}

Sprite "WSKIE0", 960, 700
{
  XScale 3.000
  YScale 3.800
  Offset -2, -28
  Patch "WSKIE0", 398, 347
}

Sprite "WSKIF0", 960, 700
{
  XScale 3.000
  YScale 3.800
  Offset -2, -28
  Patch "WSKIF0", 586, 388
}

Sprite "WSKIG0", 960, 700
{
  XScale 3.000
  YScale 3.800
  Offset -2, -28
  Patch "WSKIG0", 726, 493
}

Sprite "WCRRA0", 960, 720
{
  XScale 3.000
  YScale 3.600
  Patch "WCRRA0", 164, 317
}

Sprite "WCRFA0", 1000, 720
{
  XScale 3.000
  YScale 3.600
  Patch "WCRFA0", 158, 315
}

Sprite "WCRFB0", 1000, 720
{
  XScale 3.000
  YScale 3.600
  Patch "WCRFB0", 147, 317
}

Sprite "WCRFC0", 1000, 720
{
  XScale 3.000
  YScale 3.600
  Patch "WCRFC0", 137, 286
}

Sprite "WCRFD0", 1000, 720
{
  XScale 3.000
  YScale 3.600
  Patch "WCRFD0", -3, 350
}

Sprite "WCRFE0", 960, 720
{
  XScale 3.000
  YScale 3.600
  Patch "WCRFE0", 79, 390
}

Sprite "WCRFF0", 960, 720
{
  XScale 3.000
  YScale 3.600
  Patch "WCRFF0", 133, 375
}

Sprite "WCRLA0", 1200, 720
{
  XScale 3.000
  YScale 3.600
  Patch "WCRLA0", 159, 292
}

Sprite "WCRLB0", 1200, 720
{
  XScale 3.000
  YScale 3.600
  Patch "WCRLB0", 226, 268
}

Sprite "WCRLC0", 1200, 720
{
  XScale 3.000
  YScale 3.600
  Patch "WCRLC0", 260, 252
}

Sprite "WCRLD0", 1200, 720
{
  XScale 3.000
  YScale 3.600
  Patch "WCRLD0", 260, 252
}

Sprite "WCRLE0", 1200, 720
{
  XScale 3.000
  YScale 3.600
  Patch "WCRLE0", 260, 252
}

Sprite "WCRLF0", 1200, 720
{
  XScale 3.000
  YScale 3.600
  Patch "WCRLF0", 260, 242
}

Sprite "WCRLG0", 1200, 720
{
  XScale 3.000
  YScale 3.600
  Patch "WCRLG0", 244, 249
}

Sprite "WCRLH0", 1200, 720
{
  XScale 3.000
  YScale 3.600
  Patch "WCRLH0", 132, 270
}

Sprite "WCLRA0", 960, 720
{
  XScale 3.000
  YScale 3.800
  Patch "WCLRA0", 272, 339
}

Sprite "WCLRB0", 960, 720
{
  XScale 3.000
  YScale 3.800
  Patch "WCLRB0", 272, 339
}

Sprite "WCLFA0", 960, 720
{
  XScale 3.000
  YScale 3.800
  Patch "WCLFA0", 274, 321
}

Sprite "WCLFB0", 960, 720
{
  XScale 3.000
  YScale 3.800
  Patch "WCLFB0", 280, 321
}

Sprite "WCLFC0", 960, 720
{
  XScale 3.000
  YScale 3.800
  Patch "WCLFC0", 285, 241
}

Sprite "WCLFD0", 960, 720
{
  XScale 3.000
  YScale 3.800
  Patch "WCLFD0", 293, 228
}

Sprite "WCLFE0", 960, 900
{
  XScale 3.000
  YScale 3.800
  Offset 0, 90
  Patch "WCLFE0", 259, 178
}

Sprite "WCLLA0", 1200, 720
{
  XScale 3.000
  YScale 3.600
  Patch "WCLLA0", 290, 259
}

Sprite "WCLLB0", 1200, 720
{
  XScale 3.000
  YScale 3.600
  Patch "WCLLB0", 386, 294
}

Sprite "WCLLC0", 1200, 720
{
  XScale 3.000
  YScale 3.600
  Patch "WCLLC0", 369, 302
}

Sprite "WCLLD0", 1200, 720
{
  XScale 3.000
  YScale 3.600
  Patch "WCLLD0", 325, 315
}

Sprite "WCLLE0", 1200, 720
{
  XScale 3.000
  YScale 3.600
  Patch "WCLLE0", 305, 314
}

Sprite "WCLLF0", 1200, 720
{
  XScale 3.000
  YScale 3.600
  Patch "WCLLF0", 275, 324
}

Sprite "WCLLG0", 1200, 720
{
  XScale 3.000
  YScale 3.600
  Patch "WCLLG0", 407, 289
}

Sprite "WCLLH0", 1200, 720
{
  XScale 3.000
  YScale 3.600
  Patch "WCLLH0", 266, 273
}

Sprite "WPORA0", 1200, 720
{
  XScale 2.900
  YScale 3.400
  Patch "WPORA0", 352, 337
}

Sprite "WPOFA0", 1200, 720
{
  XScale 2.900
  YScale 3.400
  Patch "WPOFA0", 332, 359
}

Sprite "WPOFB0", 1200, 720
{
  XScale 2.900
  YScale 3.400
  Patch "WPOFB0", 305, 384
}

Sprite "WPOFC0", 1200, 720
{
  XScale 2.900
  YScale 3.400
  Offset -28, -1
  Patch "WPOFC0", 297, 362
}

Sprite "WPOFD0", 1200, 720
{
  XScale 2.900
  YScale 3.400
  Patch "WPOFD0", 346, 346
}

Sprite "WPOLA0", 1200, 720
{
  XScale 2.900
  YScale 3.400
  Patch "WPOLA0", 375, 407
}

Sprite "WPOLB0", 1200, 720
{
  XScale 2.900
  YScale 3.400
  Patch "WPOLB0", 276, 445
}

Sprite "WPOLC0", 1200, 720
{
  XScale 2.900
  YScale 3.400
  Patch "WPOLC0", 234, 466
}

Sprite "WPOLD0", 1200, 720
{
  XScale 2.900
  YScale 3.400
  Patch "WPOLD0", 227, 518
}

Sprite "WPOLE0", 1200, 720
{
  XScale 2.900
  YScale 3.400
  Patch "WPOLE0", 374, 532
}

Sprite "WHERA0", 960, 600
{
  XScale 4.000
  YScale 4.500
  Offset -311, -164
  Patch "WHERA0", 455, 170
}

Sprite "WHERB0", 960, 600
{
  XScale 4.000
  YScale 4.500
  Offset -311, -164
  Patch "WHERB0", 455, 166
}

Sprite "WHERC0", 960, 600
{
  XScale 4.000
  YScale 4.500
  Offset -311, -164
  Patch "WHERC0", 455, 166
}

Sprite "WHEFA0", 960, 600
{
  XScale 4.000
  YScale 4.500
  Offset -320, -164
  Patch "WHEFA0", 381, 143
}

Sprite "WHEFB0", 960, 600
{
  XScale 4.000
  YScale 4.500
  Offset -320, -164
  Patch "WHEFB0", 378, 99
}

Sprite "WHEFC0", 960, 600
{
  XScale 4.000
  YScale 4.500
  Offset -320, -164
  Patch "WHEFC0", 281, 18
}

Sprite "WHEFD0", 960, 600
{
  XScale 4.000
  YScale 4.500
  Offset -320, -164
  Patch "WHEFD0", 293, 43
}

Sprite "WHEFE0", 960, 600
{
  XScale 4.000
  YScale 4.500
  Offset -320, -164
  Patch "WHEFE0", 414, 159
}

Sprite "WHEFF0", 960, 600
{
  XScale 4.000
  YScale 4.500
  Offset -320, -164
  Patch "WHEFF0", 422, 218
}

Sprite "WHEFG0", 960, 600
{
  XScale 4.000
  YScale 4.500
  Offset -320, -164
  Patch "WHEFG0", 482, 341
}

Sprite "WHEFH0", 960, 600
{
  XScale 4.000
  YScale 4.500
  Offset -320, -164
  Patch "WHEFH0", 624, 502
}

Sprite "WHEFI0", 960, 600
{
  XScale 4.000
  YScale 4.500
  Offset -320, -164
  Patch "WHEFI0", 547, 362
}

Sprite "WHEFJ0", 960, 600
{
  XScale 4.000
  YScale 4.500
  Offset -320, -164
  Patch "WHEFJ0", 453, 231
}

Sprite "WHEFK0", 960, 600
{
  XScale 4.000
  YScale 4.500
  Offset -320, -164
  Patch "WHEFK0", 412, 164
}

Sprite "WHEFL0", 960, 600
{
  XScale 4.000
  YScale 4.500
  Offset -320, -164
  Patch "WHEFL0", 368, 71
}

Sprite "WHEFM0", 960, 600
{
  XScale 4.000
  YScale 4.500
  Offset -320, -164
  Patch "WHEFM0", 407, 172
}

Sprite "WHEFN0", 960, 600
{
  XScale 4.000
  YScale 4.500
  Offset -320, -164
  Patch "WHEFN0", 350, 244
}

Sprite "WHEFO0", 960, 600
{
  XScale 4.000
  YScale 4.500
  Offset -320, -164
  Patch "WHEFO0", 328, 195
}

Sprite "WHEFP0", 960, 600
{
  XScale 4.000
  YScale 4.500
  Offset -320, -164
  Patch "WHEFP0", 266, 209
}


// Paingun
Sprite "WPAFA0", 960, 600
{
  XScale 3.000
  YScale 3.600
  Offset 0, -40
  Patch "WPAFA0", 169, 257
}

Sprite "WPAFB0", 960, 600
{
  XScale 3.000
  YScale 3.600
  Offset 0, -40
  Patch "WPAFB0", 170, 249
}

Sprite "WPAFC0", 960, 600
{
  XScale 3.000
  YScale 3.600
  Offset 0, -40
  Patch "WPAFC0", 170, 252
}

Sprite "WPARA0", 960, 600
{
  XScale 3.000
  YScale 3.600
  Offset 0, -40
  Patch "WPARA0", 185, 266
}

Sprite "WPFLA0", 960, 600
{
  XScale 3.000
  YScale 3.600
  Offset 0, -40
  Patch "WPFLA0", 407, 115
}

Sprite "WPFLB0", 960, 600
{
  XScale 3.000
  YScale 3.600
  Offset 0, -40
  Patch "WPFLB0", 391, 94
}

Sprite "WPFLC0", 960, 600
{
  XScale 3.000
  YScale 3.600
  Offset 0, -40
  Patch "WPFLC0", 439, 178
}

Sprite "WPFLD0", 960, 600
{
  XScale 3.000
  YScale 3.600
  Offset 0, -40
  Patch "WPFLD0", 454, 202
}

Sprite "WPFLE0", 960, 600
{
  XScale 3.000
  YScale 3.600
  Offset 0, -40
  Patch "WPFLE0", 461, 170
}

Sprite "WPFLF0", 960, 600
{
  XScale 3.000
  YScale 3.600
  Offset 0, -40
  Patch "WPFLF0", 454, 186
}

Sprite "WHAFA0", 1400, 605
{
  XScale 3.000
  YScale 3.500
  Offset 40, 0
  Patch "WHAFA0", 7, 182
}

Sprite "WHAFB0", 1250, 605
{
  XScale 3.000
  YScale 3.500
  Offset 40, 0
  Patch "WHAFB0", 14, 36
}

Sprite "WHAFC0", 1200, 605
{
  XScale 3.000
  YScale 3.500
  Offset 40, 0
  Patch "WHAFC0", 22, 163
}

Sprite "WHAFD0", 1100, 605
{
  XScale 3.000
  YScale 3.500
  Offset 40, 0
  Patch "WHAFD0", 23, 265
}

Sprite "WHAFE0", 960, 605
{
  XScale 3.000
  YScale 3.500
  Offset 40, 0
  Patch "WHAFE0", 13, 255
}

Sprite "WHAFF0", 960, 605
{
  XScale 3.000
  YScale 3.500
  Offset 80, 0
  Patch "WHAFF0", 17, 255
}

Sprite "WHASA0", 960, 605
{
  XScale 3.000
  YScale 3.500
  Offset 80, 0
  Patch "WHASA0", 130, 245
}

Sprite "WHASB0", 960, 605
{
  XScale 3.000
  YScale 3.500
  Patch "WHASB0", 109, 217
}

Sprite "WHASC0", 960, 605
{
  XScale 3.000
  YScale 3.500
  Patch "WHASC0", 133, 175
}

Sprite "WHASD0", 960, 605
{
  XScale 3.000
  YScale 3.500
  Patch "WHASD0", 210, 150
}

Sprite "WHASE0", 960, 605
{
  XScale 3.000
  YScale 3.500
  Patch "WHASE0", 278, 149
}

Sprite "WHARA0", 960, 605
{
  XScale 3.000
  YScale 3.500
  Offset 40, 0
  Patch "WHARA0", 0, 246
}
