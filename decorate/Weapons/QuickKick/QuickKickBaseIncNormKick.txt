  Spawn:
    //TNT1 A 0 NoDelay A_k8ConLog(va("Kick spawned! Damage=%s", Damage))
    //TNT1 A 0
    TNT1 A 2
    Stop

  Death:
    //TNT1 A 0 NoDelay A_k8ConLog("Kick DEATH!")
    TNT1 A 0 A_JumpIf((IsMonster(AAPTR_TRACER) != 0) && (GetHealth(AAPTR_TRACER) <= 0), "KilledMonsterNorm")
    //TNT1 A 0 A_k8ConLog(va("%C: Kicked %C, Health=%s (%s)", GetSelf, GetTracer, GetHealth(AAPTR_TRACER), GetSpawnHealth(AAPTR_TRACER)))
    Goto DeathMain

  KilledMonsterNorm:
    TNT1 A 0 A_JumpIf(IsPlayer(AAPTR_TARGET) != 0, "ZanKilledMonsterNorm")
    Goto DeathMain

  ZanKilledMonsterNorm:
    //TNT1 A 0 A_k8ConLog(va("%C: Killed %C, Health=%s (%s), Master=%C, Target=%C", GetSelf, GetTracer, GetHealth(AAPTR_TRACER), GetSpawnHealth(AAPTR_TRACER), GetMaster, GetTarget))
    TNT1 A 0
      {
        if (CountInv("Hellclaw", AAPTR_TARGET)) {
          A_GiveInventory("HellEnergy", max(1, random(max(1, GetSpawnHealth(AAPTR_TRACER)/18), max(1, GetSpawnHealth(AAPTR_TRACER)/14+6))/4)*(HasBerserk(AAPTR_TARGET) ? 3 : 1), AAPTR_TARGET);
        }
      }
    Goto DeathMain
