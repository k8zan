//**************************************************************************
//**
//**  Copyright (C) 2021-2022 Ketmar Dark
//**
//**  This program is free software: you can redistribute it and/or modify
//**  it under the terms of the GNU General Public License as published by
//**  the Free Software Foundation, version 3 of the License ONLY.
//**
//**  This program is distributed in the hope that it will be useful,
//**  but WITHOUT ANY WARRANTY; without even the implied warranty of
//**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//**  GNU General Public License for more details.
//**
//**  You should have received a copy of the GNU General Public License
//**  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//**
//**************************************************************************
class Zan_LineSpecialGameInfo : replaces(lastchild of LineSpecialGameInfo);


//==========================================================================
//
//  PostPlayerClassInit
//
//==========================================================================
override void PostPlayerClassInit () {
  ::PostPlayerClassInit();

  CreateCvarBool('__k8ZanActive', false, "...", CVAR_ROM);

  class rpl = GetClassReplacement(FindClass('DoomPlayer'));
  //printdebug("DoomPlayer replacement: %C", rpl);
  if (GetClassName(rpl) != 'Zan') {
    printerror("*** Please, load PlayerZan mod as the last one!");
    SetShadowCvarB('__k8ZanActive', false);
  } else {
    printdebug("PlayerZan mod is loaded.");
    SetShadowCvarB('__k8ZanActive', true);
  }
  printdebug("%s player class%s cleared", PlayerClasses.length, (PlayerClasses.length != 1 ? "es" : ""));
  PlayerClasses.length = 1;
  PlayerClasses[0] = FindClass('Zan');
}


override void Init () {
  ::Init();

  // enable headshots
  SetShadowCvarB('k8HSEnabled', true);
  // distance multiplier (the more -- the less headshot chance on big distances)
  SetShadowCvarF('k8HSDistMult', 1.1);
  // enable criticals
  SetShadowCvarB('k8HSCriticals', true);
  // don't allow headshots from all projectiles
  SetShadowCvarB('k8HSAnyProjectile', false);
  // fastkill/instakill some annoying monsters
  SetShadowCvarB('k8HSFastKillAnnoyingMonsters', true);
  // instakill Pain Elementals
  SetShadowCvarB('k8HSInstaKillPainElementals', true);
  // instakill Lost Souls
  SetShadowCvarB('k8HSInstaKillLostSouls', true);
  // how mush damage should be inflicted to Arch-Vile
  SetShadowCvarI('k8HSFastKillArchVileDamage', /*230*/66);

  SetShadowCvarB('plr_allow_damage_thrust', true);
  SetShadowCvarI('compat_nopassover', 2); // never
  SetShadowCvarB('sv_decoration_block_projectiles', false);
  SetShadowCvarB('gm_fix_projectile_attack_offset', true);
  SetShadowCvarB('sv_ignore_nojump', true);
  SetShadowCvarB('sv_ignore_nocrouch', true);
  SetShadowCvarB('sv_ignore_nomlook', true);
  SetShadowCvarB('sv_disable_jump', false);
  SetShadowCvarB('sv_disable_crouch', false);
  SetShadowCvarB('sv_disable_mlook', false);

  // pain flash
  SetShadowCvarF('k8DamageFlashMaxIntensity', 0.3);
}
