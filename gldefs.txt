// PROJECTILES

//***WEAPONS & AMMO***
Brightmap sprite WFRAB0 {
  map "brightmaps/Weapons/BM_WFRAB0.png"
}
Brightmap sprite AFRAA0 {
  map "brightmaps/Weapons/BM_AFRAA0.png"
}
Brightmap sprite AFRAD0 {
  map "brightmaps/Weapons/BM_AFRAD0.png"
}
Brightmap sprite WCRYB0 {
  map "brightmaps/Weapons/BM_WCRYB0.png"
}
Brightmap sprite ABOLA0 {
  map "brightmaps/Weapons/BM_ABOLA0.png"
}
Brightmap sprite APOXA0 {
  map "brightmaps/Weapons/BM_APOXA0.png"
}
Brightmap sprite APOXB0 {
  map "brightmaps/Weapons/BM_APOXB0.png"
}


//PROJECTILES
Object SpikeShot {
  Frame PSPI { Light ZanHotSpikeGlow }
}

Object SpikeShotAimed {
  Frame PSPI { Light ZanHotSpikeGlow }
}

Object FlameShot {
  Frame PFRA { Light ZanFlameShotBig }
}

Object FragShot {
  Frame PFRA { Light ZanHotSpikeGlow }
}

Object PaingunTracer {
  Frame PTRA { Light ZanHotSpikeGlow }
}

Object Cryball {
  Frame PCRY { Light ZanCryballShot }
}

Object Cryring {
  Frame PCRY { Light ZanCryballShot }
}

Object HellBall {
  Frame PHEB { Light ZanHellBallShot }
}

Object HellMine {
  Frame PHEM { Light ZanHellBallShot }
}

Object FireBolt {
  Frame PBOL { Light ZanHotSpikeGlow }
}

Object ImpactBolt {
  Frame PBOL { Light ZanHotSpikeGlow }
}


//FIRES
Object FireTinyLooping {
  Frame LFLA { Light ZanFireTinyFlicker }
}

Object FireTinyDie1 {
  Frame LFLA { Light ZanFireTinyFlicker }
}

Object FireTinyDie2 {
  Frame LFLA { Light ZanFireTinyFlicker }
}

Object FireTinyDie3 {
  Frame LFLA { Light ZanFireTinyFlicker }
}


//LIGHTS
//SpikeShot, FragShot
PointLight ZanHotSpikeGlow {
  color 1.0 0.5 0.0
  size 25
  //offset 0 16 0
  dontlightself 1
  noshadowmap 1
}

//Flame Projectile, Cerberus Breath, Fire Bolt, Burning Monsters
PointLight ZanFlameShotBig {
  color 1.0 0.6 0.0
  size 60
  dontlightself 1
  noshadowmap 1
}

//Cryball Projectile
PointLight ZanCryballShot {
  color 0.75 0.6 0.0
  size 64
  dontlightself 1
  noshadowmap 1
}

PointLight ZanHellBallShot {
  color 1.0 0.1 0.1
  size 36
  dontlightself 1
  noshadowmap 1
}

FlickerLight ZanFireTinyFlicker {
  Color 0.6 0.5 0.0
  Size 10
  SecondarySize 11
  Chance 0.6
  dontlightself 1
  noshadowmap 1
}

FlickerLight ZanHellMineFlicker {
  Color 0.7 0.02 0.02
  Size 24
  SecondarySize 28
  Chance 0.3
  dontlightself 1
  noshadowmap 1
}


PointLight ZanHugeExplosion {
  Color 1.0 0.3 0.1
  Size 96
  dontlightself 1
}

PointLight ZanLargeExplosion {
  Color 1.0 0.3 0.1
  Size 64
  dontlightself 1
}

PointLight ZanSmallExplosion {
  Color 1.0 0.3 0.1
  Size 42
  dontlightself 1
}
